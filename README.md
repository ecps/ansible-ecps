## New installation procedures for ECPS Workstations

#### Ubuntu 20.04 (Focal Fossa) Installation process
- Restart Workstation and Press F12 to choose Boot device

![Boot Device](/img/image-20221118473.png "Boot Device")

- Once the system boots on the live OS, the following screen will appear, choose Install Ubuntu

![Boot Image](/img/image-20221018150029191.png "Boot")

- Choose keyboard layout

![Keyboard](/img/image-20221018151115045.png "Keyboard layout")

- Installation type - We choose normal for a full Desktop experience

![Desktop Install](/img/image-20221018151325116.png "Full Desktop Install")

- Customize the installation media, choose Erase Disk and install Ubuntu and on Advanced features choose Use LVM for the new Ubuntu Installation.

![LVM Disk](/img/image-202211118478.png "Custom Installation")

- Click on continue to write the changes to disk

![Disk partition](/img/image-20221118479.png "Write Changes to disk")

- Choose Time Zone to Zurich and press on Continue

![Time Zone](/img/image-20221018153018768.png "Time Zone Selection")

- Create first user.  Note that this user has sudo rights.  

![Sudo User](/img/image-20221018153427466.png "Sudo User")

- Relax and let the installation continue until you see this screen

![Installation](/img/image-20221118483.png "Installation Process")

- Once the installation is complete, remove the bootable media (USB key) and press Enter to reboot

![Reboot](/img/image-202211118484.png "Reboot to complete")

- Once installation is done, log on as the Sudo user you created in the step above, complete the initial setup screen 
- We will move next to configure the Network interface - Click on settings on the menu

![network](/img/image-202211118492.png "Configure interface")

![Activate](/img/image-202211118497.png "Activate interface")

![Activate2](/img/image-202211118498.png "Activated interface")

- Click on the button next to the activate button to configure the network IPV4 settings.  Choose manual to add your IP

![Wired](/img/image-202211118494.png "Configure Wired settings")

![IPAddress](/img/image-202211118495.png "Add fixed IP address")

- Move down this window to add the DNS addresses and click on Apply

![DNS](/img/image-202211118496.png "Add DNS address")

- Once the desktop is connected to the wired network, we will complete the post-installation. Press Ctrl+Alt+F3 to open a terminal, log on as the sudo user you created

- Run:

````
sudo su -  
apt update
apt install -y git software-properties-common python3-pip
pip install ansible
git clone https://gitlab.epfl.ch/ecps/ansible-ecps /tmp/ansible-ecps
cd /tmp/ansible-ecps
echo "PASSWORD_GIVEN_TO_YOU_BY_STI_IT" > .vault
ansible-galaxy collection install -r requirements.yml
ansible-galaxy install -r requirements.yml
ansible-playbook playbook.yml
````
- Reboot the machine by typing: reboot